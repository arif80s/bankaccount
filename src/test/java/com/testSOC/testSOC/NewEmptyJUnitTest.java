/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.testSOC.testSOC;

import com.business.BankAcc_Service_I;
import com.business.BankAccount_Service_Impl;
import com.controller.BankAcc_Controller;
import com.model.UserAccountModel;
import com.persistence.BankAcc_persistence_Impl;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author abra
 */
public class NewEmptyJUnitTest {

    public NewEmptyJUnitTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
//    @Test
    public void createAccount_service() {
        UserAccountModel um = new UserAccountModel();
        um.setId(9);
        um.setAcc_Creation_Date("1/88/17");
        um.setAcc_HolderName("Joy");
        um.setAcc_Number(1240);
        um.setAcc_address("shewrapara, dhaka");
        um.setAcc_balance(88800);
        um.setAcc_nomineeInfo("talha, mirpur, Dhaka");

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spconfig.xml");
        BankAcc_Service_I ba = (BankAcc_Service_I) applicationContext.getBean("bean_BankAcc_Service_Impl");

//        BankAccount_Service_Impl bs = new BankAccount_Service_Impl();
        ba.createAccount(um);

    }

//    @Test
    public void testDeposit_service() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spconfig.xml");
        BankAcc_Service_I ba = (BankAcc_Service_I) applicationContext.getBean("bean_BankAcc_Service_Impl");
        ba.depositAccount(1235, 50000000);

    }
//    @Test

    public void withdrawAccount_service() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spconfig.xml");
        BankAcc_Service_I ba = (BankAcc_Service_I) applicationContext.getBean("bean_BankAcc_Service_Impl");
        ba.withdrawAccount(1234, 653);

    }

//    @Test
    public void transferService() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spconfig.xml");
        BankAcc_Service_I ba = (BankAcc_Service_I) applicationContext.getBean("bean_BankAcc_Service_Impl");
        ba.transferAccount(1235, 1234, 1000);
    }

//    @Test
    public void selectId_ByAccNo_persistence() {
        BankAcc_persistence_Impl bpi = new BankAcc_persistence_Impl();
        System.out.println("id------->" + bpi.selectId_ByAccNo(1238));

    }
//    @Test

    public void selectBalance_ById_persistence() {
        BankAcc_persistence_Impl bpi = new BankAcc_persistence_Impl();
        System.out.println("balance------->" + bpi.selectBalance_ById(3));

    }
//    @Test

    public void update_persistence() {
        BankAcc_persistence_Impl bpi = new BankAcc_persistence_Impl();
        bpi.update(4, 99999);

    }
//    @Test

    public void delete_persistence() {
        BankAcc_persistence_Impl bpi = new BankAcc_persistence_Impl();
        bpi.delete(4);

    }

//    @Test
    public void objectExist_persistence() {
        BankAcc_persistence_Impl bpi = new BankAcc_persistence_Impl();
        bpi.objectExist(111);

    }
//    @Test

    public void create_persistence() {
        UserAccountModel um = new UserAccountModel();
        um.setId(8);
        um.setAcc_Creation_Date("90/88/17");
        um.setAcc_HolderName("Asef");
        um.setAcc_Number(1239);
        um.setAcc_address("shewrapara, dhaka");
        um.setAcc_balance(440009);
        um.setAcc_nomineeInfo("mamun, mirpur, Dhaka");
        BankAcc_persistence_Impl bpi = new BankAcc_persistence_Impl();
        bpi.create(um);

    }

//    @Test
    public void transferAccountPersistence() {
        BankAcc_persistence_Impl bpi = new BankAcc_persistence_Impl();
        bpi.transferAccount(1, 55555, 2, 10000);
    }
//    @Test

    public void testTransferController() {

    }

//    @Test
//    public void getAccController() {
//        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spconfig.xml");
//        BankAcc_Controller bc = (BankAcc_Controller) applicationContext.getBean("bean_BankAcc_Controller");
////        UserAccountModel ubm = bc.getAccount(1234);
//        System.out.println("Nameeeeeee::" + ubm.getAcc_HolderName());
//    }

    @Test
    public void showController() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spconfig.xml");
        BankAcc_Service_I ba = (BankAcc_Service_I) applicationContext.getBean("bean_BankAcc_Service_Impl");
        UserAccountModel usm = ba.showAccount(1235);
        System.out.println("nameee:::"+ usm.getAcc_HolderName());
        System.out.println("nameee:::"+ usm.getAcc_address());
    }

}
