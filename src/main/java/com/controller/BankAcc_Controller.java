/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.business.BankAcc_Service_I;
import com.model.UserAccountModel;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abra
 */
@RestController
public class BankAcc_Controller {

    /**
     * @return the bankAcc_Service_I
     */
//    public BankAcc_Service_I getBankAcc_Service_I() {
//        return bankAcc_Service_I;
//    }
//
//    /**
//     * @param bankAcc_Service_I the bankAcc_Service_I to set
//     */
//    public void setBankAcc_Service_I(BankAcc_Service_I bankAcc_Service_I) {
//        this.bankAcc_Service_I = bankAcc_Service_I;
//    }
//
////    @Autowired
//    private BankAcc_Service_I bankAcc_Service_I;
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spconfig.xml");
    BankAcc_Service_I ba = (BankAcc_Service_I) applicationContext.getBean("bean_BankAcc_Service_Impl");

    @RequestMapping(value = "/account/{accNo}")
//    public UserAccountModel getAccount(@PathVariable int accNo) {
    public String getAccount(@PathVariable int accNo) {
        System.out.println("pathvariable is::::"+accNo);
        System.out.println("\n \n ");
        System.out.println("inside controller");
        System.out.println("\n \n ");
//        return ba.showAccount(accNo);
         UserAccountModel uam = ba.showAccount(accNo);
         String name = uam.getAcc_HolderName();
         System.out.println("name isssss::::"+name);
        return name;
    }

 

//    @RequestMapping(method = RequestMethod.POST, value = "/createAcc")
//    public void createAccount(@RequestBody UserAccountModel userAccount) {
//        getBankAcc_Service_I().createAccount(userAccount);
//    }
//
//    @RequestMapping(method = RequestMethod.PUT, value = "/depositAcc")
//    public void depositAccount(int accNo, int depositAmount) {
//        getBankAcc_Service_I().depositAccount(accNo, depositAmount);
//    }
//
//    @RequestMapping(method = RequestMethod.DELETE, value = "/withdraw")
//    public void withdrawAccount(int accNo, int withdrawAmount) {
//        getBankAcc_Service_I().withdrawAccount(accNo, withdrawAmount);
//    }
//
//    @RequestMapping(method = RequestMethod.PUT, value = "/transfer")
//    public void transferAccount(int senderAccNo, int receiverAccNo, int amount) {
//        bankAcc_Service_I.transferAccount(senderAccNo, receiverAccNo, amount);
//    }


}
