/*
 * To change this license header, choose License Headembers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

/**
 *
 * @author abra
 */
public class UserAccountModel {

    /**
     * @return the acc_Number
     */
    private int version;
    private int id;
    private int acc_Number;
    private String acc_HolderName;
    private String acc_address;
    private String acc_Creation_Date;
    private int acc_balance;
    private String acc_nomineeInfo;

    public UserAccountModel() {
    }

    public UserAccountModel(int version, int id, int acc_Number, String acc_HolderName, String acc_address, String acc_Creation_Date, int acc_balance, String acc_nomineeInfo) {
        this.version = version;
        this.id = id;
        this.acc_Number = acc_Number;
        this.acc_HolderName = acc_HolderName;
        this.acc_address = acc_address;
        this.acc_Creation_Date = acc_Creation_Date;
        this.acc_balance = acc_balance;
        this.acc_nomineeInfo = acc_nomineeInfo;
    }
    
    

    /**
     * @return the version
     */
    public int getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    public int getAcc_Number() {
        return acc_Number;
    }

    /**
     * @param acc_Number the acc_Number to set
     */
    public void setAcc_Number(int acc_Number) {
        this.acc_Number = acc_Number;
    }

    /**
     * @return the acc_HolderName
     */
    public String getAcc_HolderName() {
        return acc_HolderName;
    }

    /**
     * @param acc_HolderName the acc_HolderName to set
     */
    public void setAcc_HolderName(String acc_HolderName) {
        this.acc_HolderName = acc_HolderName;
    }

    /**
     * @return the acc_address
     */
    public String getAcc_address() {
        return acc_address;
    }

    /**
     * @param acc_address the acc_address to set
     */
    public void setAcc_address(String acc_address) {
        this.acc_address = acc_address;
    }

    /**
     * @return the acc_Creation_Date
     */
    public String getAcc_Creation_Date() {
        return acc_Creation_Date;
    }

    /**
     * @param acc_Creation_Date the acc_Creation_Date to set
     */
    public void setAcc_Creation_Date(String acc_Creation_Date) {
        this.acc_Creation_Date = acc_Creation_Date;
    }

    /**
     * @return the acc_balance
     */
    public int getAcc_balance() {
        return acc_balance;
    }

    /**
     * @param acc_balance the acc_balance to set
     */
    public void setAcc_balance(int acc_balance) {
        this.acc_balance = acc_balance;
    }

    /**
     * @return the acc_nomineeInfo
     */
    public String getAcc_nomineeInfo() {
        return acc_nomineeInfo;
    }

    /**
     * @param acc_nomineeInfo the acc_nomineeInfo to set
     */
    public void setAcc_nomineeInfo(String acc_nomineeInfo) {
        this.acc_nomineeInfo = acc_nomineeInfo;
    }

}
