/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.persistence;

import com.model.UserAccountModel;
import org.springframework.stereotype.Service;

/**
 *
 * @author abra
 */
@Service
public interface BankAcc_Persistence_I {
    public void create(UserAccountModel userAccountModel);
    public int update(int id, int depositAmount);
    public int selectBalance_ById(int id);
    public int selectId_ByAccNo(int accNo);
    public int delete(int id);
    public boolean objectExist(int id);
    public int transferAccount(int senderId, int senderAmount, int receiverId, int receiverAmont);

    public UserAccountModel showAccount(int Id);
    
    
 
    
}