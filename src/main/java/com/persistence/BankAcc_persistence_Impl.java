/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.persistence;

import com.model.UserAccountModel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abra
 */
public class BankAcc_persistence_Impl implements BankAcc_Persistence_I {

    /**
     * @return the bankAcc_Persistence_I
     */
//    BankAcc_persistence_Impl bpi = new BankAcc_persistence_Impl();
    @Override
    public void create(UserAccountModel userAccountModel) {
        Configuration cfg = new Configuration();
        cfg.configure("bankAcc.cfg.xml");

        SessionFactory factory = cfg.buildSessionFactory();
        Session session = factory.openSession();

        Transaction tx = session.beginTransaction();
        session.save(userAccountModel);
        tx.commit();
        session.close();
        factory.close();
    }

    @Override
    public int update(int id, int depositAmount) {
        Configuration cfg = new Configuration();
        cfg.configure("bankAcc.cfg.xml");

        SessionFactory factory = cfg.buildSessionFactory();
        Session session = factory.openSession();

        Object o = session.load(UserAccountModel.class, new Integer(id));
        UserAccountModel userAccModelObj = (UserAccountModel) o;

        Transaction tx = session.beginTransaction();

        userAccModelObj.setAcc_balance(depositAmount);

        tx.commit();

        System.out.println("Object Updated successfully.....!!");
        session.close();
        factory.close();
        return 1;
    }

    @Override
    public int selectBalance_ById(int id) {
        Configuration cfg = new Configuration();
        cfg.configure("bankAcc.cfg.xml");

        SessionFactory factory = cfg.buildSessionFactory();
        Session session = factory.openSession();

        Object o = session.load(UserAccountModel.class, new Integer(id));
        UserAccountModel userAccModelObj = (UserAccountModel) o;
        System.out.println("balance inside method::" + userAccModelObj.getAcc_balance());
        return userAccModelObj.getAcc_balance();
    }

    @Override
    public int selectId_ByAccNo(int accNo) {
        System.out.println("I am selectId_ByAccNo method");
        Configuration cfg = new Configuration();
        cfg.configure("bankAcc.cfg.xml");

        SessionFactory factory = cfg.buildSessionFactory();
        Session session = factory.openSession();

        //citeria
        Criteria criteria = session.createCriteria(UserAccountModel.class)
                .add(Restrictions.eq("acc_Number", new Integer(accNo)));

        // Convenience method to return a single instance that matches the
        // query, or null if the query returns no results.
        List<UserAccountModel> o = criteria.list();

//        System.out.println("\n before casting object \n"+ o.get(0));
//        UserAccountModel user = (UserAccountModel) o.get(0);
//        UserAccountModel user = o.get(0);
//          System.out.println("nammez"+o.get(0));

//        System.out.println("iddd"+userAccModelObj.getId());
//        System.out.println("iddd"+userAccModelObj.getAcc_Number());
//        System.out.println("adress:::"+userAccModelObj.getAcc_address());
        System.out.println("\n before return \n");
//        return user.getId();
        return o.get(0).getId();
//        return 0;
    }

    @Override
    public int delete(int id) {
        Configuration cfg = new Configuration();
        cfg.configure("bankAcc.cfg.xml");

        SessionFactory factory = cfg.buildSessionFactory();
        Session session = factory.openSession();

        Object o = session.load(UserAccountModel.class, new Integer(id));
        UserAccountModel userAccModelObj = (UserAccountModel) o;

        Transaction tx = session.beginTransaction();
        session.delete(userAccModelObj);
        tx.commit();
        session.close();
        factory.close();
        return 1;

    }

    @Override
    public boolean objectExist(int id) {
        boolean b;
        Configuration cfg = new Configuration();
        cfg.configure("bankAcc.cfg.xml");

        SessionFactory factory = cfg.buildSessionFactory();
        Session session = factory.openSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(UserAccountModel.class);
        criteria.add(Restrictions.eq("id", id));
        criteria.setProjection(Projections.rowCount());
        long count = (Long) criteria.uniqueResult();
        session.getTransaction().commit();
        if (count != 0) {
            b = true;
        } else {
            b = false;
        }
        return b;
    }

    @Override
    public int transferAccount(int senderId, int senderAmount, int receiverId, int receiverAmont) {
        Configuration cfg = new Configuration();
        cfg.configure("bankAcc.cfg.xml");

        Session session = null;
        Transaction tx = null;
        int flug;

        SessionFactory factory = cfg.buildSessionFactory();

        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            tx.setTimeout(5);

            //update
            update(senderId, senderAmount);
            update(receiverId, receiverAmont);

            //doSomething(session);
            tx.commit();
            flug = 1;

        } catch (RuntimeException e) {
            try {
                tx.rollback();
                flug = 0;
            } catch (RuntimeException rbe) {
                //log.error("Couldn’t roll back transaction", rbe);
            }
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return flug;

    }

    @Override
    public UserAccountModel showAccount(int Id) {
        System.out.println("\n Inside showaccount persistence \n ");

//        UserAccountModel usr;
        Configuration cfg = new Configuration();
        cfg.configure("bankAcc.cfg.xml");

        SessionFactory factory = cfg.buildSessionFactory();
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();

        Object o = session.load(UserAccountModel.class, new Integer(Id));
        UserAccountModel userAccModel = (UserAccountModel) o;
//        usr = userAccModel;
        // For loading Transaction scope is not necessary...
//        System.out.println("Loaded object UserAccountModel name is___" + s.getProName());

//        System.out.println("Object Loaded successfully.....!!");
        Hibernate.initialize(userAccModel);
        tx.commit();
        session.close();
        factory.close();
        System.out.println("user" + userAccModel.getAcc_HolderName());
        return userAccModel;
    }

}
