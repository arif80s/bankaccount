/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.business;

import com.model.UserAccountModel;
import com.persistence.BankAcc_Persistence_I;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author abra
 */
public class BankAccount_Service_Impl implements BankAcc_Service_I {

    /**
     * @return the bankAcc_Persistence_I
     */
//    @Autowired
    private BankAcc_Persistence_I bankAcc_Persistence_I;

    public BankAcc_Persistence_I getBankAcc_Persistence_I() {
        return bankAcc_Persistence_I;
    }

    /**
     * @param bankAcc_Persistence_I the bankAcc_Persistence_I to set
     */
    public void setBankAcc_Persistence_I(BankAcc_Persistence_I bankAcc_Persistence_I) {
        this.bankAcc_Persistence_I = bankAcc_Persistence_I;
    }

    @Override
    public void createAccount(UserAccountModel userAccount) {
        getBankAcc_Persistence_I().create(userAccount);
    }

    @Override
    public void depositAccount(int accNo, int depositAmount) {
        int previousBalance;
        int id;
        id = bankAcc_Persistence_I.selectId_ByAccNo(accNo);
        previousBalance = bankAcc_Persistence_I.selectBalance_ById(id);
        System.out.println("current balance :::" + previousBalance);

        bankAcc_Persistence_I.update(id, depositAmount + previousBalance);
    }

    @Override
    public int withdrawAccount(int accNo, int withdrawAmount) {
        int previousBalance;
        int id;
        int afterWithdraw;
        int flug;
        id = bankAcc_Persistence_I.selectId_ByAccNo(accNo);
        System.out.println("idddddd::::" + id);
        previousBalance = bankAcc_Persistence_I.selectBalance_ById(id);
        System.out.println("previous balance is::::" + previousBalance);
        afterWithdraw = previousBalance - withdrawAmount;
        System.out.println("after withdraw isss:::" + afterWithdraw);

        if (withdrawAmount > 10000 || afterWithdraw < 500) {
            flug = 0;
        } else {
            flug = bankAcc_Persistence_I.update(id, afterWithdraw);

        }
        return flug;

    }

    @Override
    public int transferAccount(int senderAccNo, int receiverAccNo, int amount) {
        int flug;
        int senderId;
        int receiverId;
        int senderPreviousBalance;
        int receiverPreviousBalance;
        int amountAfterSending;
        int amountAfterReceiving;
        boolean objectExist;
        senderId = bankAcc_Persistence_I.selectId_ByAccNo(senderAccNo);
        receiverId = bankAcc_Persistence_I.selectId_ByAccNo(receiverAccNo);
        senderPreviousBalance = bankAcc_Persistence_I.selectBalance_ById(senderId);
        receiverPreviousBalance = bankAcc_Persistence_I.selectBalance_ById(receiverId);
        amountAfterSending = senderPreviousBalance - amount;
        amountAfterReceiving = receiverPreviousBalance + amount;
        objectExist = bankAcc_Persistence_I.objectExist(receiverId);
        if (objectExist) {
            if (amount <= 10000 && amountAfterSending >= 500) {
                flug = bankAcc_Persistence_I.transferAccount(senderId, amountAfterSending, receiverId, amountAfterReceiving);
            } else {
                flug = 0;
            }
        } else {
            flug = 0;
        }
        return flug;
    }

    @Override
    public UserAccountModel showAccount(int accNo) {
        int userId;
        System.out.println("\n \n ");
        System.out.println("inside showAccount service impl");
        System.out.println("\n \n ");
        userId = bankAcc_Persistence_I.selectId_ByAccNo(accNo);
        System.out.println("\n \n after userId\n \n");
        return bankAcc_Persistence_I.showAccount(userId);
    }

}
