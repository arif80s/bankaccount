/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.business;

import com.model.UserAccountModel;
import org.springframework.stereotype.Service;

/**
 *
 * @author abra
 */
//@Service
public interface BankAcc_Service_I {
    public void createAccount(UserAccountModel userAccount); 
    public void depositAccount(int accNo, int depositAmount); 
    public int withdrawAccount(int accNo, int withdrawAmount); 
    public int transferAccount(int senderAccNo, int receiverAccNo, int amount);
    public UserAccountModel showAccount(int accNo);

}
