package com.testSOC.testSOC;

import com.controller.BankAcc_Controller;
import com.controller.TopicController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = BankAcc_Controller.class)
public class TestSocApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestSocApplication.class, args);
	}
}
